#!/usr/bin/env python
# -----
# Script for decrypting MabiNovel file 'TemporarilyData.data'.
# -----

import struct, sys

key = [0x33, 0x30, 0x36, 0x24, 0x33, 0x2e, 0x70, 0x60,
       0x01, 0x62, 0x1f, 0x67, 0x6b, 0x1f, 0x67, 0x6b,
       0x69, 0x68, 0x60, 0x01, 0x62, 0x65, 0x1f, 0x6b,
       0x67, 0x6b]
key = bytes([x ^ 0x41 for x in key])

def xorpad(src, pad):
	dest = bytearray(len(src))
	for i in range(len(dest)):
		dest[i] = src[i] ^ pad[i % len(pad)]
	return dest

def to_raw(str_bytes):
	bom = struct.unpack('>H', str_bytes[:2])[0]
	if bom == 0xFFFE:
		return xorpad(str_bytes[2:].decode('utf-16-le').encode('iso-8859-1'), key)
	else:
		raise ValueError('Unexpected BOM: 0x{:04X}'.format(bom))

def from_raw(raw_bytes):
	data = bytearray(struct.pack('>H', 0xFFFE)) # Add BOM
	data.extend(xorpad(raw_bytes, key).decode('iso-8859-1').encode('utf-16-le'))
	return data

if len(sys.argv) >= 4:
	op = sys.argv[1].lower()
	if op == 'd': # Decrypt
		with open(sys.argv[2], 'rb') as infile:
			data = to_raw(infile.read())
			with open(sys.argv[3], 'wb') as outfile:
				outfile.write(data)
	elif op == 'e': # Encrypt
		with open(sys.argv[2], 'rb') as infile:
			data = from_raw(infile.read())
			with open(sys.argv[3], 'wb') as outfile:
				outfile.write(data)
	else:
		raise ValueError('Unknown operation: {}'.format(op))
else:
	print('usage:', sys.argv[0], '[d|e] <infile> <outfile>')
