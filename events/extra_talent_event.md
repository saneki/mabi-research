Extra Talent Event
==================

More info is [here](http://wiki.mabinogiworld.com/view/Extra_Talent_Event).

Example Packets
---------------

Example Request:
```
Op: 0000AC27, Id: 00100000000XXXXX
001 [..............68] Byte   : 104 // Treasure Hunter
```

Example Response:
```
Op: 0000AC28, Id: 00100000000XXXXX
001 [..............68] Byte   : 104 // Treasure Hunter
```

The response is also sent upon connecting.

Talent Ids
----------

| Talent          | Id   |
|-----------------|------|
| Adventure       | 0x00 |
| Close Combat    | 0x01 |
| Magic           | 0x02 |
| Archery         | 0x03 |
| Mercantile      | 0x04 |
| Battle Alchemy  | 0x05 |
| Martial Arts    | 0x06 |
| Music           | 0x07 |
| Puppetry        | 0x08 |
| Lance Combat    | 0x09 |
| Holy Arts       | 0x0A |
| Transmutation   | 0x0B |
| Cooking         | 0x0C |
| Smithing        | 0x0D |
| Tailoring       | 0x0E |
| Medicine        | 0x0F |
| Carpentry       | 0x10 |
| Gunslinger      | 0x11 |
| Ninja           | 0x12 |
| Merlin          | 0x64 |
| Soul Star       | 0x65 |
| Vates           | 0x66 |
| Culinary Artist | 0x67 |
| Treasure Hunter | 0x68 |
