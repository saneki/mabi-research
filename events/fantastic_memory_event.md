Fantastic Memory Event
======================

Event Duration: 2019/08/15 - 2019/10/10

More info: https://wiki.mabinogiworld.com/view/Fantastic_Memory_Relay_Event

Packets `0xAD85` and `0xAD86`
-----------------------------

`AD85` is a request sent when clicking the event button to get related info.

It is also sent when clicking a different tab in the event GUI, in this case the request packet is
empty (it does not send your character Id).

On a melody tick (30-minutes), a `0xAD86` response is sent to the client (without any request)
with the updated melody information (this seems to happen regardless of whether the event GUI is
open or not).

```
Op: 0000AD85, Id: 0010000000XXXXXX

00001 [0010000000XXXXXX] Long   : <redacted>
```

The response is sent in `AD86`:

```
Op: 0000AD86, Id: 0010000000XXXXXX

00001 [..............04] Byte   : 4              # Tab count (4).
00002 [..............02] Byte   : 2              # Tab[0] completions.
00003 [..............05] Byte   : 5              # Tab[1] completions.
00004 [..............00] Byte   : 0              # Tab[2] completions.
00005 [..............00] Byte   : 0              # Tab[3] completions.
00006 [........0000001F] Int    : 31             # Current melody count.
00007 [........00000000] Int    : 0              # Melodies obtained today?
00008 [000039F00BFB0E67] Long   : 63703155936871 # Current time?
00009 [000039F00BF5DCE0] Long   : 63703155596512 # Time of previous melody tick?
00010 [..............05] Byte   : 5
00011 [..............00] Byte   : 0
00012 [..............00] Byte   : 0
00013 [..............00] Byte   : 0
00014 [..............00] Byte   : 0
00015 [..............00] Byte   : 0
00016 [..............01] Byte   : 1
```

Other `AD86` packet samples from around the same time:

```
Op: 0000AD86, Id: 0010000000XXXXXX

00001 [..............04] Byte   : 4
00002 [..............02] Byte   : 2
00003 [..............05] Byte   : 5
00004 [..............00] Byte   : 0
00005 [..............00] Byte   : 0
00006 [........0000001F] Int    : 31
00007 [........00000000] Int    : 0
00008 [000039F00C007217] Long   : 63703156290071
00009 [000039F00BF5DCE0] Long   : 63703155596512
00010 [..............05] Byte   : 5
00011 [..............00] Byte   : 0
00012 [..............00] Byte   : 0
00013 [..............00] Byte   : 0
00014 [..............00] Byte   : 0
00015 [..............00] Byte   : 0
00016 [..............01] Byte   : 1

Op: 0000AD86, Id: 0010000000XXXXXX

00001 [..............04] Byte   : 4
00002 [..............02] Byte   : 2
00003 [..............05] Byte   : 5
00004 [..............00] Byte   : 0
00005 [..............00] Byte   : 0
00006 [........0000001F] Int    : 31
00007 [........00000000] Int    : 0
00008 [000039F00C01091B] Long   : 63703156328731
00009 [000039F00BF5DCE0] Long   : 63703155596512
00010 [..............05] Byte   : 5
00011 [..............00] Byte   : 0
00012 [..............00] Byte   : 0
00013 [..............00] Byte   : 0
00014 [..............00] Byte   : 0
00015 [..............00] Byte   : 0
00016 [..............01] Byte   : 1

// ... After melody tick (31 => 32) ...

Op: 0000AD86, Id: 0010000000XXXXXX

00001 [..............04] Byte   : 4
00002 [..............02] Byte   : 2
00003 [..............05] Byte   : 5
00004 [..............00] Byte   : 0
00005 [..............00] Byte   : 0
00006 [........00000020] Int    : 32
00007 [........00000001] Int    : 1
00008 [000039F00C136121] Long   : 63703157530913
00009 [000039F00C115445] Long   : 63703157396549
00010 [..............05] Byte   : 5
00011 [..............00] Byte   : 0
00012 [..............00] Byte   : 0
00013 [..............00] Byte   : 0
00014 [..............00] Byte   : 0
00015 [..............00] Byte   : 0
00016 [..............01] Byte   : 1
```

Packets `0x660B` and `0x660C`
-----------------------------

For sending referral codes using the "Invite friends and collect Melodies!" GUI.

### Response Type `0`

Request:

```
Op: 0000660B, Id: 0010000000XXXXXX

00001 [................] String : Nao@Saneki
```

Response:

```
Op: 0000660C, Id: 0010000000XXXXXX

00001 [..............00] Byte   : 0  // Whether or not it was successful?
00002 [..............00] Byte   : 0  // Error code? Indicates which error dialogue to show.
```

Messagebox: "Failed to enter invitation code. Make sure you have the right code and try again."

### Response Type `1`

Request:

```
Op: 0000660B, Id: 0010000000XXXXXX

00001 [................] String : Saneki
```

Response:

```
Op: 0000660C, Id: 0010000000XXXXXX

00001 [..............00] Byte   : 0
00002 [..............01] Byte   : 1
```

Messagebox: "You cannot refer characters who are on a different server."

This seems to always be the response if a server prefix is not provided (such as `Nao@`).

### Response Type `3`

Request:

```
Op: 0000660B, Id: 0010000000XXXXXX

00001 [................] String : Nao@Blah
```

Response:

```
Op: 0000660C, Id: 0010000000XXXXXX

00001 [..............00] Byte   : 0
00002 [..............03] Byte   : 3
```

Messagebox: "Characters must be in the same channel to enter the referral code."

This seems to always be the response if a server prefix is provided, but there is no character by
the provided name. It seems to happen even when only inputting `Nao@` or `Nao`.

Packets `0x660D` and `0x660E`
-----------------------------

The request `0x660D` is empty:

```
Op: 0000660D, Id: 0010000000XXXXXX
```

The response `0x660E`:

```
Op: 0000660E, Id: 0010000000XXXXXX

00001 [..............01] Byte   : 1              # ???
00002 [........00000020] Int    : 32             # Current melody count.
00003 [................] String :                # Might be used for custom invitation code?
00004 [..............00] Byte   : 0              # ???
00005 [........00000001] Int    : 1              # Melodies obtained today
00006 [000039F00C115445] Long   : 63703157396549 # Time of previous melody tick.

// ... After melody tick (32 => 33) ...

Op: 0000660E, Id: 0010000000XXXXXX

00001 [..............01] Byte   : 1
00002 [........00000021] Int    : 33
00003 [................] String :
00004 [..............00] Byte   : 0
00005 [........00000002] Int    : 2
00006 [000039F00C2CCBA9] Long   : 63703159196585
```

It isn't mentioned in the packet, but the GUI for this packet displays:

"Your invitation code: Nao@Saneki"

This implies that all invitation codes are (by default): `<Server-name>@<Character-name>`

It's possible that the string in this packet is for overriding the default invitation code?
