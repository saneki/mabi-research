Mission Point Event
===================

More info is [here](http://wiki.mabinogiworld.com/view/Mission_Point_Event).

The actual quest infos themselves are sent with the others via the
ChannelCharacterInfoRequestR (0x5209) packet.

Packet flow when clicking mission point button:
```
> Send MissionPointInit    (0xAC59)
> Send MissionPointInfo    (0xAC53)
> Send MissionPointPrizes  (0xAC4F)
> Recv MissionPointInitR   (0xAC5A)
> Recv MissionPointInfoR   (0xAC54)
> Recv MissionPointPrizesR (0xAC50)
```

Upon completing a mission point quest, a CompleteQuest packet is sent by the
client and before anything else a 0xAC55 packet is sent in response with the
players new mission point and reset counts.


MissionPointInit?
-----------------

Request Example:
```
Op: 0000AC59, Id: 00100000000XXXXX

001 [........00032891] Int    : 206993
```

Response Example:
```
Op: 0000AC5A, Id: 00100000000XXXXX

001 [........00032891] Int    : 206993
002 [........00000002] Int    : 2
003 [........00013881] Int    : 80001
004 [........00013882] Int    : 80002
```


MissionPointInfo
----------------

Request Example:
```
Op: 0000AC53, Id: 00100000000XXXXX

001 [........00032891] Int    : 206993
```

Response Example:
```
Op: 0000AC54, Id: 00100000000XXXXX

001 [........00032891] Int    : 206993 // ???
002 [........0000001E] Int    : 30     // Mission points count
003 [............0003] Short  : 3      // Remaining resets
004 [000039D86A2F3D80] Long   : 63601657200000
005 [000039D919477580] Long   : 63604594800000
```


MissionPointUpdateR
-------------------

Example (response):
```
Op: 0000AC55, Id: 00100000000XXXXX

001 [........0000003C] Int    : 60 // New mission points count
002 [............0003] Short  : 3  // New remaining resets
```


MissionPointQuestReset
----------------------

Request Example:
```
Op: 0000AC57, Id: 00100000000XXXXX

001 [006000XXXXXXXXXX] Long   : <redacted> // Quest Id
```

Response Example:
```
Op: 0000AC58, Id: 00100000000XXXXX

001 [..............01] Byte   : 1 // Success?
002 [............0002] Short  : 2 // Remaining resets
```


MissionPointPurchasePrize
-------------------------

Request Example:
```
Op: 0000AC51, Id: 00100000000XXXXX

001 [............000E] Short  : 14 // Prize index
```

Response Example:
```
Op: 0000AC52, Id: 00100000000XXXXX

001 [..............01] Byte   : 1 // Success?
```

Packet Flow:
```
> Send request  MissionPointPurchasePrize  (0xAC51)
> Recv response ItemNew
> Recv Response ItemUpdate
> Recv response AcquireInfo
> Recv response MissionPointUpdateR        (0xAC55)
> Recv response MissionPointPurchasePrizeR (0xAC52)
```


MissionPointPrizes
------------------

Request Example:
```
Op: 0000AC4F, Id: 00100000000XXXXX

001 [........00032891] Int    : 206993
```

Response Example:
```
Op: 0000AC50, Id: 00100000000XXXXX

001 [........00032891] Int    : 206993 // ???
002 [..............01] Byte   : 1      // ???
003 [........0000000E] Int    : 14     // Number of following 4-field entries
                                       // Probably the 14 prizes in the "Point Shop" tab

004 [............0001] Short  : 1      // Index?
005 [........00013BEF] Int    : 80879  // Item Id?
006 [................] String :        // Expire time as string?
007 [........00001388] Int    : 5000   // Price in mission points

008 [............0002] Short  : 2
009 [........00013BF0] Int    : 80880
010 [................] String : 
011 [........00001388] Int    : 5000
012 [............0003] Short  : 3
013 [........00013C0A] Int    : 80906
014 [................] String : 
015 [........00001388] Int    : 5000
016 [............0004] Short  : 4
017 [........00013C0B] Int    : 80907
018 [................] String : 
019 [........00001388] Int    : 5000
020 [............0005] Short  : 5
021 [........0000705D] Int    : 28765
022 [................] String : 
023 [........00000FA0] Int    : 4000
024 [............0006] Short  : 6
025 [........0000705C] Int    : 28764
026 [................] String : 
027 [........00000FA0] Int    : 4000
028 [............0007] Short  : 7
029 [........00012921] Int    : 76065
030 [................] String : 
031 [........000009C4] Int    : 2500
032 [............0008] Short  : 8
033 [........0000704F] Int    : 28751
034 [................] String : 
035 [........000005DC] Int    : 1500
036 [............0009] Short  : 9
037 [........00003018] Int    : 12312
038 [................] String : expire:10080
039 [........00000258] Int    : 600
040 [............000A] Short  : 10
041 [........00003019] Int    : 12313
042 [................] String : expire:10080
043 [........00000258] Int    : 600
044 [............000B] Short  : 11
045 [........00014CAA] Int    : 85162
046 [................] String : expire:10080
047 [........000000FA] Int    : 250
048 [............000C] Short  : 12
049 [........00016E55] Int    : 93781
050 [................] String : expire:10080
051 [........000000C8] Int    : 200
052 [............000D] Short  : 13
053 [........00016E56] Int    : 93782
054 [................] String : expire:10080
055 [........000000C8] Int    : 200
056 [............000E] Short  : 14
057 [........00016E57] Int    : 93783
058 [................] String : 
059 [........00000078] Int    : 120
```


Example of a daily (30 point) quest:
```
//
// Item info:
//
1002 [005000XXXXXXXXXX] Long   : <redacted>
1003 [..............02] Byte   : 2
1004 [................] Bin    : 17 00 00 00 88 11 01 00 91 91 FF 00 2C 0E AB 00
                                19 9D 29 00 00 00 00 00 00 00 00 00 00 00 00 00
                                00 00 00 00 00 00 00 00 02 00 00 00 00 00 00 00
1005 [................] Bin    : 01 00 5F 00 00 00 00 00 00 00 00 00 00 00 00 00
                                00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
                                00 00 00 00 00 00 6F 00 00 00 00 00 00 00 00 00
                                02 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
                                00 00 69 00 00 00 00 00 FF FF FF FF 00 00 00 00
                                00 00 00 00 00 00 31 00 00 00 00 00 00 00 00 00
                                17 4F 01 00
1006 [................] String : QSTTIP:s:N_Conquer Peaca Dungeon!|D_Clear Peaca Dungeon.|A_|R_Reward%C 30 points|T_0;
1007 [................] String : 
1008 [..............00] Byte   : 0
1009 [006000XXXXXXXXXX] Long   : <redacted> // Quest Id (first long in quest info)
1010 [..............00] Byte   : 0
1011 [..............00] Byte   : 0
//
// Quest info:
//
4957 [006000XXXXXXXXXX] Long   : <redacted>
4958 [..............00] Byte   : 0
4959 [005000XXXXXXXXXX] Long   : <redacted>
4960 [..............13] Byte   : 19
4961 [........0001388A] Int    : 80010
4962 [................] String : Conquer Peaca Dungeon!
4963 [................] String : Clear Peaca Dungeon.
4964 [................] String : 
4965 [........00000000] Int    : 0
4966 [........00011188] Int    : 70024
4967 [..............00] Byte   : 0
4968 [..............00] Byte   : 0
4969 [..............00] Byte   : 0
4970 [..............00] Byte   : 0
4971 [..............00] Byte   : 0
4972 [..............01] Byte   : 1
4973 [..............00] Byte   : 0
4974 [..............00] Byte   : 0
4975 [..............00] Byte   : 0
4976 [..............00] Byte   : 0
4977 [..............00] Byte   : 0
4978 [..............00] Byte   : 0
4979 [........0000000D] Int    : 13
4980 [........0000002A] Int    : 42
4981 [........00000000] Int    : 0
4982 [................] String : 
4983 [........00000000] Int    : 0
4984 [........00000000] Int    : 0
4985 [................] String : <xml soundset="4" npc=""/>
4986 [................] String : QMAMEXP:f:1.000000;QMAMGLD:f:1.000000;QMBEXP:f:1.000000;QMBGLD:f:1.000000;QMBHDCTADD:4:0;QMGNRB:f:1.000000;QMSMEE:f:0.000000;QMSMEXP:f:1.000000;QMSMGLD:f:1.000000;
4987 [........00000000] Int    : 0
4988 [........00000000] Int    : 0
4989 [........00000001] Int    : 1
4990 [..............58] Byte   : 88
4991 [................] String : Clear Peaca Dungeon on any difficulty except Abyss Peaca Advanced
4992 [................] String : QOD1:s:Senmag_Peaca_;QODK:1:10;TARGETCOUNT:4:1;
4993 [........00000000] Int    : 0
4994 [........00000000] Float  : 0.0
4995 [..............00] Byte   : 0
4996 [..............01] Byte   : 1
4997 [..............00] Byte   : 0
4998 [..............01] Byte   : 1
4999 [..............00] Byte   : 0
5000 [..............00] Byte   : 0
5001 [..............01] Byte   : 1
5002 [..............01] Byte   : 1
5003 [..............10] Byte   : 16
5004 [................] String : Reward: 30 points
5005 [..............00] Byte   : 0
5006 [..............01] Byte   : 1
5007 [..............01] Byte   : 1
5008 [..............00] Byte   : 0
```


Example of a weekly (100 point) quest:
```
//
// Item info:
//
882 [005000XXXXXXXXXX] Long   : <redacted>
883 [..............02] Byte   : 2
884 [................] Bin    : 17 00 00 00 88 11 01 00 91 91 FF 00 2C 0E AB 00
                                19 9D 29 00 00 00 00 00 00 00 00 00 00 00 00 00
                                00 00 00 00 00 00 00 00 02 00 00 00 00 00 00 00
885 [................] Bin    : 01 47 37 14 00 00 00 00 00 00 00 00 00 00 00 00
                                00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
                                00 00 00 00 00 00 D0 00 00 00 00 00 00 00 00 00
                                02 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
                                00 00 00 00 00 00 00 00 FF FF FF FF 00 00 00 00
                                00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
                                17 4F 01 00
886 [................] String : QSTTIP:s:N_Points for Points|D_Aggressively collect Mission Points.|A_|R_Reward%C 100 points|T_0;
887 [................] String : 
888 [..............00] Byte   : 0
889 [006000XXXXXXXXXX] Long   : <redacted>
890 [..............00] Byte   : 0
891 [..............00] Byte   : 0
//
// Quest info:
//
4247 [006000XXXXXXXXXX] Long   : <redacted>
4248 [..............00] Byte   : 0
4249 [005000XXXXXXXXXX] Long   : <redacted>
4250 [..............13] Byte   : 19
4251 [........000138DC] Int    : 80092
4252 [................] String : Points for Points
4253 [................] String : Aggressively collect Mission Points.
4254 [................] String : 
4255 [........00000000] Int    : 0
4256 [........00011188] Int    : 70024
4257 [..............00] Byte   : 0
4258 [..............00] Byte   : 0
4259 [..............00] Byte   : 0
4260 [..............00] Byte   : 0
4261 [..............00] Byte   : 0
4262 [..............01] Byte   : 1
4263 [..............00] Byte   : 0
4264 [..............00] Byte   : 0
4265 [..............00] Byte   : 0
4266 [..............00] Byte   : 0
4267 [..............00] Byte   : 0
4268 [..............00] Byte   : 0
4269 [........0000000D] Int    : 13
4270 [........0000002B] Int    : 43
4271 [........00000000] Int    : 0
4272 [................] String : 
4273 [........00000000] Int    : 0
4274 [........00000000] Int    : 0
4275 [................] String : <xml soundset="4" npc=""/>
4276 [................] String : QMAMEXP:f:1.000000;QMAMGLD:f:1.000000;QMBEXP:f:1.000000;QMBGLD:f:1.000000;QMBHDCTADD:4:0;QMGNRB:f:1.000000;QMSMEE:f:0.000000;QMSMEXP:f:1.000000;QMSMGLD:f:1.000000;
4277 [........00000000] Int    : 0
4278 [........00000000] Int    : 0
4279 [........00000001] Int    : 1
4280 [..............58] Byte   : 88
4281 [................] String : Complete Daily Missions 40 times.
4282 [................] String : QODK:1:12;TARGETCOUNT:4:40;
4283 [........00000005] Int    : 5
4284 [........00000000] Float  : 0.0
4285 [..............00] Byte   : 0
4286 [..............01] Byte   : 1
4287 [..............00] Byte   : 0
4288 [..............01] Byte   : 1
4289 [..............00] Byte   : 0
4290 [..............00] Byte   : 0
4291 [..............01] Byte   : 1
4292 [..............01] Byte   : 1
4293 [..............10] Byte   : 16
4294 [................] String : Reward: 100 points
4295 [..............00] Byte   : 0
4296 [..............01] Byte   : 1
4297 [..............01] Byte   : 1
4298 [..............00] Byte   : 0
```

Example of a monthly (300 point) quest:
```
//
// Item info:
//
972 [005000XXXXXXXXXX] Long   : <redacted>
973 [..............02] Byte   : 2
974 [................] Bin    : 17 00 00 00 88 11 01 00 91 91 FF 00 2C 0E AB 00
                                19 9D 29 00 00 00 00 00 00 00 00 00 00 00 00 00
                                00 00 00 00 00 00 00 00 02 00 00 00 00 00 00 00
975 [................] Bin    : 01 02 53 0D 00 00 00 00 00 00 00 00 00 00 00 00
                                00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
                                00 00 00 00 00 00 50 00 00 00 00 00 00 00 00 00
                                02 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
                                00 00 00 00 00 00 00 00 FF FF FF FF 00 00 00 00
                                00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
                                17 4F 01 00
976 [................] String : QSTTIP:s:N_Rollin' in Points|D_Keep clearing Weekly Missions.|A_|R_Reward%C 300 points|T_0;
977 [................] String : 
978 [..............00] Byte   : 0
979 [006000XXXXXXXXXX] Long   : <redacted>
980 [..............00] Byte   : 0
981 [..............00] Byte   : 0
//
// Quest info:
//
4793 [006000XXXXXXXXXX] Long   : <redacted>
4794 [..............00] Byte   : 0
4795 [005000XXXXXXXXXX] Long   : <redacted>
4796 [..............13] Byte   : 19
4797 [........000138FA] Int    : 80122
4798 [................] String : Rollin' in Points
4799 [................] String : Keep clearing Weekly Missions.
4800 [................] String : 
4801 [........00000000] Int    : 0
4802 [........00011188] Int    : 70024
4803 [..............00] Byte   : 0
4804 [..............00] Byte   : 0
4805 [..............00] Byte   : 0
4806 [..............00] Byte   : 0
4807 [..............00] Byte   : 0
4808 [..............01] Byte   : 1
4809 [..............00] Byte   : 0
4810 [..............00] Byte   : 0
4811 [..............00] Byte   : 0
4812 [..............00] Byte   : 0
4813 [..............00] Byte   : 0
4814 [..............00] Byte   : 0
4815 [........0000000D] Int    : 13
4816 [........0000002C] Int    : 44
4817 [........00000000] Int    : 0
4818 [................] String : 
4819 [........00000000] Int    : 0
4820 [........00000000] Int    : 0
4821 [................] String : <xml soundset="4" npc=""/>
4822 [................] String : QMAMEXP:f:1.000000;QMAMGLD:f:1.000000;QMBEXP:f:1.000000;QMBGLD:f:1.000000;QMBHDCTADD:4:0;QMGNRB:f:1.000000;QMSMEE:f:0.000000;QMSMEXP:f:1.000000;QMSMGLD:f:1.000000;
4823 [........00000000] Int    : 0
4824 [........00000000] Int    : 0
4825 [........00000001] Int    : 1
4826 [..............58] Byte   : 88
4827 [................] String : Complete Weekly Missions 20 times.
4828 [................] String : QODK:1:13;TARGETCOUNT:4:20;
4829 [........00000000] Int    : 0
4830 [........00000000] Float  : 0.0
4831 [..............00] Byte   : 0
4832 [..............01] Byte   : 1
4833 [..............00] Byte   : 0
4834 [..............01] Byte   : 1
4835 [..............00] Byte   : 0
4836 [..............00] Byte   : 0
4837 [..............01] Byte   : 1
4838 [..............01] Byte   : 1
4839 [..............10] Byte   : 16
4840 [................] String : Reward: 300 points
4841 [..............00] Byte   : 0
4842 [..............01] Byte   : 1
4843 [..............01] Byte   : 1
4844 [..............00] Byte   : 0
```
