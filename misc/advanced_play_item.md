Advanced Play Item
==================

When an advanced play item is available, this packet is sent between packets
0x6603 and PointsUpdate (0x4E90) when logging in:

```
Op: 0000909F, Id: 00100000000XXXXX

001 [..............06] Byte   : 6
002 [................] String : id:63028 expire:10080|id:63047 count:2|id:63229 count:2
```


AdvPlayItemGet
--------------

Response example:
```
Op: 0000909C, Id: 00100000000XXXXX

001 [..............00] Byte   : 0
002 [........000B3BA3] Int    : 736163
```


AdvPlayItemChoose
-----------------

Request example:
```
Op: 000090A0, Id: 00100000000XXXXX

001 [................] String :   // Always empty string?
002 [..............01] Byte   : 1 // Always 1?
```

Response example:
```
Op: 000090A1, Id: 00100000000XXXXX

<empty>
```

Packet flow when choosing an advanced play item:
```
> Send AdvPlayItemChoose  (0x90A0)
> Recv AdvPlayItemGet     (0x909C)
> Recv ItemNew            (0x59E0)
> Recv ItemUpdate         (0x5BD4)
> Recv AcquireInfo        (0x5271)
> Recv QuestOwlComplete   (0x9094)
> Recv Notice             (0x526D)
> Recv AdvPlayItemChooseR (0x90A1)
```

Example Notice packet:
```
Op: 0000526D, Id: 00100000000XXXXX

001 [..............04] Byte   : 4
002 [................] String : You have received today's advanced play item.
```
