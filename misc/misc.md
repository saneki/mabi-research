Misc
====

Client sends empty 0xAAF7 packet when prompted "do you want to skip the movie?".
Clicking "No" sends nothing.


Lorna's Stadium Horn
--------------------

Example request:
```
Op: 0001FC20, Id: 0010000000XXXXXX

<empty>
```

Example primary response:
```
Op: 0001FC21, Id: 0010000000XXXXXX

001 [........00000001] Int    : 1
002 [........00000000] Int    : 0 // Probably picture Id
003 [................] String : Lorna
004 [................] String : A graveyard! It's crawling with Spiders.
There's no better place to gather Spider webs!
```

Example secondary response:
```
Op: 0001FC22, Id: 0010000000XXXXXX

001 [................] String : talk
```

After the client sends an ItemMove request when equipping or unequipping the
horn, the server immediately responds to tell the client to start or stop
sending 0x1FC20 requests (this response is sent before the ItemMoveInfo response).

Sent to tell client to start sending periodic 0x1FC20 requests:
```
Op: 0001FC0A, Id: 0010000000XXXXXX

001 [005000XXXXXXXXXX] Long   : <redacted> // Item Id of horn
```

Sent to tell client to stop sending those requests:
```
Op: 0001FC0B, Id: 0010000000XXXXXX

001 [005000XXXXXXXXXX] Long   : <redacted> // Item Id of horn
```
