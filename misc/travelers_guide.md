Traveler's Guide
================


DailyMissions
-------------

- This request is sent on the first login of the day.
  - Seems to be sent right after the `EventInform` (`0x88B8`) request?
- Response contains the names of daily shadow missions (for Tara and Taillteann).

Request example:

```
Op: 0000AB70, Id: 0010000000XXXXXX

<empty>
```

Response example:

```
Op: 0000AB71, Id: 0010000000XXXXXX

00001 [................] String : Shadow Cast City         // Daily Tara mission
00002 [................] String : Defeat Fomor Commander I // Daily Taillteann mission

```


Warping
-------

When using the Moon Gate / Mana Tunnel transport system via the Traveler's Guide, it sends a
request before prompting for a cost (in gold).

Request:

```
Op: 0000AB7C, Id: 0010000000XXXXXX

00001 [........00004F72] Int    : 20338  // Location Id of destination
```

Response:

```
Op: 0000AB7D, Id: 0010000000XXXXXX

00001 [..............01] Byte   : 1      // Maybe success bool?
00002 [........00004F45] Int    : 20293  // Location Id of current area (Dunbarton)
00003 [........00004F72] Int    : 20338  // Location Id of destination
00004 [........0000249A] Int    : 9370   // Cost in gold
```

When trying to teleport to the Dunbarton while in the general area of Dunbarton, there is still a
gold cost albeit relatively small (300g):

Request:

```
Op: 0000AB7C, Id: 0010000000XXXXXX

00001 [........00004F45] Int    : 20293
```

Response:

```
Op: 0000AB7D, Id: 0010000000XXXXXX

00001 [..............01] Byte   : 1
00002 [........00004F45] Int    : 20293
00003 [........00004F45] Int    : 20293
00004 [........0000012C] Int    : 300
```

On confirming the warp, the client sends a different request:

```
Op: 0000AB75, Id: 0010000000XXXXXX

00001 [........00004F45] Int    : 20293  // Location Id of destination
```

Response (on success):

```
Op: 0000AB76, Id: 0010000000XXXXXX

00001 [........00004F45] Int    : 20293
```

The (mostly) full packet flow during this warp from what I've seen:

```
> TravelGuideWarpConfirm  (0xAB75)
< ItemAmount              (0x59EE)  // Updates gold amount in pouch after purchase
< SetLocation             (0x6594)
< WarpUnk1                (0x4E39)
< WarpUnk2                (0x526E)
< ???                     (0x69B7)
< EntityDisappears        (0x520D)
< EntitiesDisappear       (0x5335)
< CharacterLock           (0x701E)
< EnterRegion             (0x6597)
< Weather                 (0x1ADB0)
< ???                     (0xAC97)
< WarpUnk3                (0xA8AF)
< TravelGuideWarpConfirmR (0xAB76)
> EnterRegionRequest      (0x6598)
< CharacterUnlock         (0x701F)
...
```


Unknown
-------

After the DailyMissions request packet on login, another request packet is immediately sent which
is likely related to the Traveler's Guide (based on the similar Op code).

Request example:

```
Op: 0000AB79, Id: 0010000000XXXXXX

00001 [........00000005] Int    : 5
```

Response example:

```
Op: 0000AB7A, Id: 0010000000XXXXXX

00001 [..............01] Byte   : 1
```


JobsList
--------

The Traveler's Guide GUI has 3 buttons to the right which can be clicked:

- Jobs
- Raid
- Today's Contents

However, only the "Jobs" button sends a request to the server.

Request example:

```
Op: 0000AB77, Id: 0010000000XXXXXX

<empty>
```

Response example:

```
Op: 0000AB78, Id: 0010000000XXXXXX

00001 [................] String : ArbeitListSize:4:61;ENDTIME0:4:15;ENDTIME1:4:15;ENDTIME10:4:19;ENDTIME11:4:23;ENDTIME12:4:19;ENDTIME13:4:19;ENDTIME14:4:16;ENDTIME15:4:20;ENDTIME16:4:19;ENDTIME17:4:19;ENDTIME18:4:19;ENDTIME19:4:19;ENDTIME2:4:15;ENDTIME20:4:19;ENDTIME21:4:19;ENDTIME22:4:19;ENDTIME23:4:19;ENDTIME24:4:19;ENDTIME25:4:19;ENDTIME26:4:19;ENDTIME27:4:19;ENDTIME28:4:19;ENDTIME29:4:16;ENDTIME3:4:15;ENDTIME30:4:21;ENDTIME31:4:20;ENDTIME32:4:16;ENDTIME33:4:21;ENDTIME34:4:19;ENDTIME35:4:21;ENDTIME36:4:23;ENDTIME37:4:15;ENDTIME38:4:21;ENDTIME39:4:22;ENDTIME4:4:15;ENDTIME40:4:21;ENDTIME41:4:21;ENDTIME42:4:21;ENDTIME43:4:21;ENDTIME44:4:19;ENDTIME45:4:21;ENDTIME46:4:21;ENDTIME47:4:21;ENDTIME48:4:21;ENDTIME49:4:21;ENDTIME5:4:15;ENDTIME50:4:21;ENDTIME51:4:19;ENDTIME52:4:21;ENDTIME53:4:21;ENDTIME54:4:20;ENDTIME55:4:22;ENDTIME56:4:22;ENDTIME57:4:22;ENDTIME58:4:22;ENDTIME59:4:22;ENDTIME6:4:14;ENDTIME60:4:23;ENDTIME7:4:15;ENDTIME8:4:15;ENDTIME9:4:19;STARTTIME0:4:6;STARTTIME1:4:6;STARTTIME10:4:7;STARTTIME11:4:7;STARTTIME12:4:7;STARTTIME13:4:7;STARTTIME14:4:7;STARTTIME15:4:7;STARTTIME16:4:7;STARTTIME17:4:7;STARTTIME18:4:7;STARTTIME19:4:7;STARTTIME2:4:6;STARTTIME20:4:7;STARTTIME21:4:7;STARTTIME22:4:7;STARTTIME23:4:7;STARTTIME24:4:7;STARTTIME25:4:7;STARTTIME26:4:7;STARTTIME27:4:7;STARTTIME28:4:7;STARTTIME29:4:7;STARTTIME3:4:6;STARTTIME30:4:8;STARTTIME31:4:8;STARTTIME32:4:8;STARTTIME33:4:9;STARTTIME34:4:9;STARTTIME35:4:9;STARTTIME36:4:10;STARTTIME37:4:10;STARTTIME38:4:10;STARTTIME39:4:10;STARTTIME4:4:6;STARTTIME40:4:12;STARTTIME41:4:12;STARTTIME42:4:12;STARTTIME43:4:12;STARTTIME44:4:12;STARTTIME45:4:12;STARTTIME46:4:12;STARTTIME47:4:12;STARTTIME48:4:12;STARTTIME49:4:12;STARTTIME5:4:6;STARTTIME50:4:12;STARTTIME51:4:12;STARTTIME52:4:12;STARTTIME53:4:12;STARTTIME54:4:13;STARTTIME55:4:15;STARTTIME56:4:15;STARTTIME57:4:15;STARTTIME58:4:15;STARTTIME59:4:15;STARTTIME6:4:6;STARTTIME60:4:16;STARTTIME7:4:6;STARTTIME8:4:6;STARTTIME9:4:7;SmartContentBoardType:s:arbeit;TOWN0:s:Physis;TOWN1:s:Filia;TOWN10:s:Bangor;TOWN11:s:Tara;TOWN12:s:Tir Chonaill;TOWN13:s:Dugald Aisle;TOWN14:s:Dunbarton;TOWN15:s:Emain Macha;TOWN16:s:Filia;TOWN17:s:Dunbarton;TOWN18:s:Dunbarton;TOWN19:s:Emain Macha;TOWN2:s:Emain Macha;TOWN20:s:Tir Chonaill;TOWN21:s:Courcle;TOWN22:s:Emain Macha;TOWN23:s:Physis;TOWN24:s:Qilla;TOWN25:s:Zardine;TOWN26:s:Taillteann;TOWN27:s:Tir Chonaill;TOWN28:s:Taillteann;TOWN29:s:Tara;TOWN3:s:Courcle;TOWN30:s:Emain Macha;TOWN31:s:Tara;TOWN32:s:Emain Macha;TOWN33:s:Taillteann;TOWN34:s:Dunbarton;TOWN35:s:Belvast;TOWN36:s:Bangor;TOWN37:s:Emain Macha;TOWN38:s:Tara;TOWN39:s:Belvast;TOWN4:s:Tir Chonaill;TOWN40:s:Emain Macha;TOWN41:s:Filia;TOWN42:s:Physis;TOWN43:s:Bangor;TOWN44:s:Bangor;TOWN45:s:Qilla;TOWN46:s:Tara;TOWN47:s:Dunbarton;TOWN48:s:Dunbarton;TOWN49:s:Emain Macha;TOWN5:s:Tir Chonaill;TOWN50:s:Tir Chonaill;TOWN51:s:Tir Chonaill;TOWN52:s:Taillteann;TOWN53:s:Tir Chonaill;TOWN54:s:Dunbarton;TOWN55:s:Qilla;TOWN56:s:Physis;TOWN57:s:Emain Macha;TOWN58:s:Filia;TOWN59:s:Dunbarton;TOWN6:s:Belvast;TOWN60:s:Cobh;TOWN7:s:Zardine;TOWN8:s:Dunbarton;TOWN9:s:Filia;TYPE0:s:Healer's House;TYPE1:s:Healer's House;TYPE10:s:Blacksmith Shop;TYPE11:s:Bank;TYPE12:s:General;TYPE13:s:General;TYPE14:s:Adventurers' Association;TYPE15:s:Puppet Shop;TYPE16:s:Clothing Shop;TYPE17:s:Clothing Shop;TYPE18:s:General Shop;TYPE19:s:Clothing Shop;TYPE2:s:Healer's House;TYPE20:s:General Shop;TYPE21:s:Clothing Shop;TYPE22:s:General Shop;TYPE23:s:General Shop;TYPE24:s:General Shop;TYPE25:s:General Shop;TYPE26:s:Clothing Shop;TYPE27:s:Inn;TYPE28:s:General Shop;TYPE29:s:Street Artist;TYPE3:s:Healer's House;TYPE30:s:Grocery Store;TYPE31:s:Pontiff's Court;TYPE32:s:Flower Shop;TYPE33:s:Alchemist House;TYPE34:s:Library;TYPE35:s:Grocery Store;TYPE36:s:Stope;TYPE37:s:Music Shop;TYPE38:s:Jousting Arena;TYPE39:s:Pub;TYPE4:s:The Amazing Race of Mabinogi;TYPE40:s:Church;TYPE41:s:Church;TYPE42:s:Grocery Store;TYPE43:s:Church;TYPE44:s:Blacksmith Shop;TYPE45:s:Healer's House;TYPE46:s:Church;TYPE47:s:Grocery Store;TYPE48:s:Church;TYPE49:s:Grocery Store;TYPE5:s:Healer's House;TYPE50:s:Church;TYPE51:s:Blacksmith Shop;TYPE52:s:Church;TYPE53:s:Grocery Store;TYPE54:s:Bookstore;TYPE55:s:Weapons Shop;TYPE56:s:Weapons Shop;TYPE57:s:Weapons Shop;TYPE58:s:Weapons Shop;TYPE59:s:Weapons Shop;TYPE6:s:Bank;TYPE60:s:Lighthouse;TYPE7:s:Healer's House;TYPE8:s:Healer's House;TYPE9:s:General Shop;
```

Adding a newline for each entry gives us this string:

```
ArbeitListSize:4:61;
ENDTIME0:4:15;
ENDTIME1:4:15;
ENDTIME10:4:19;
ENDTIME11:4:23;
ENDTIME12:4:19;
ENDTIME13:4:19;
ENDTIME14:4:16;
ENDTIME15:4:20;
ENDTIME16:4:19;
ENDTIME17:4:19;
ENDTIME18:4:19;
ENDTIME19:4:19;
ENDTIME2:4:15;
ENDTIME20:4:19;
ENDTIME21:4:19;
ENDTIME22:4:19;
ENDTIME23:4:19;
ENDTIME24:4:19;
ENDTIME25:4:19;
ENDTIME26:4:19;
ENDTIME27:4:19;
ENDTIME28:4:19;
ENDTIME29:4:16;
ENDTIME3:4:15;
ENDTIME30:4:21;
ENDTIME31:4:20;
ENDTIME32:4:16;
ENDTIME33:4:21;
ENDTIME34:4:19;
ENDTIME35:4:21;
ENDTIME36:4:23;
ENDTIME37:4:15;
ENDTIME38:4:21;
ENDTIME39:4:22;
ENDTIME4:4:15;
ENDTIME40:4:21;
ENDTIME41:4:21;
ENDTIME42:4:21;
ENDTIME43:4:21;
ENDTIME44:4:19;
ENDTIME45:4:21;
ENDTIME46:4:21;
ENDTIME47:4:21;
ENDTIME48:4:21;
ENDTIME49:4:21;
ENDTIME5:4:15;
ENDTIME50:4:21;
ENDTIME51:4:19;
ENDTIME52:4:21;
ENDTIME53:4:21;
ENDTIME54:4:20;
ENDTIME55:4:22;
ENDTIME56:4:22;
ENDTIME57:4:22;
ENDTIME58:4:22;
ENDTIME59:4:22;
ENDTIME6:4:14;
ENDTIME60:4:23;
ENDTIME7:4:15;
ENDTIME8:4:15;
ENDTIME9:4:19;
STARTTIME0:4:6;
STARTTIME1:4:6;
STARTTIME10:4:7;
STARTTIME11:4:7;
STARTTIME12:4:7;
STARTTIME13:4:7;
STARTTIME14:4:7;
STARTTIME15:4:7;
STARTTIME16:4:7;
STARTTIME17:4:7;
STARTTIME18:4:7;
STARTTIME19:4:7;
STARTTIME2:4:6;
STARTTIME20:4:7;
STARTTIME21:4:7;
STARTTIME22:4:7;
STARTTIME23:4:7;
STARTTIME24:4:7;
STARTTIME25:4:7;
STARTTIME26:4:7;
STARTTIME27:4:7;
STARTTIME28:4:7;
STARTTIME29:4:7;
STARTTIME3:4:6;
STARTTIME30:4:8;
STARTTIME31:4:8;
STARTTIME32:4:8;
STARTTIME33:4:9;
STARTTIME34:4:9;
STARTTIME35:4:9;
STARTTIME36:4:10;
STARTTIME37:4:10;
STARTTIME38:4:10;
STARTTIME39:4:10;
STARTTIME4:4:6;
STARTTIME40:4:12;
STARTTIME41:4:12;
STARTTIME42:4:12;
STARTTIME43:4:12;
STARTTIME44:4:12;
STARTTIME45:4:12;
STARTTIME46:4:12;
STARTTIME47:4:12;
STARTTIME48:4:12;
STARTTIME49:4:12;
STARTTIME5:4:6;
STARTTIME50:4:12;
STARTTIME51:4:12;
STARTTIME52:4:12;
STARTTIME53:4:12;
STARTTIME54:4:13;
STARTTIME55:4:15;
STARTTIME56:4:15;
STARTTIME57:4:15;
STARTTIME58:4:15;
STARTTIME59:4:15;
STARTTIME6:4:6;
STARTTIME60:4:16;
STARTTIME7:4:6;
STARTTIME8:4:6;
STARTTIME9:4:7;
SmartContentBoardType:s:arbeit;
TOWN0:s:Physis;
TOWN1:s:Filia;
TOWN10:s:Bangor;
TOWN11:s:Tara;
TOWN12:s:Tir Chonaill;
TOWN13:s:Dugald Aisle;
TOWN14:s:Dunbarton;
TOWN15:s:Emain Macha;
TOWN16:s:Filia;
TOWN17:s:Dunbarton;
TOWN18:s:Dunbarton;
TOWN19:s:Emain Macha;
TOWN2:s:Emain Macha;
TOWN20:s:Tir Chonaill;
TOWN21:s:Courcle;
TOWN22:s:Emain Macha;
TOWN23:s:Physis;
TOWN24:s:Qilla;
TOWN25:s:Zardine;
TOWN26:s:Taillteann;
TOWN27:s:Tir Chonaill;
TOWN28:s:Taillteann;
TOWN29:s:Tara;
TOWN3:s:Courcle;
TOWN30:s:Emain Macha;
TOWN31:s:Tara;
TOWN32:s:Emain Macha;
TOWN33:s:Taillteann;
TOWN34:s:Dunbarton;
TOWN35:s:Belvast;
TOWN36:s:Bangor;
TOWN37:s:Emain Macha;
TOWN38:s:Tara;
TOWN39:s:Belvast;
TOWN4:s:Tir Chonaill;
TOWN40:s:Emain Macha;
TOWN41:s:Filia;
TOWN42:s:Physis;
TOWN43:s:Bangor;
TOWN44:s:Bangor;
TOWN45:s:Qilla;
TOWN46:s:Tara;
TOWN47:s:Dunbarton;
TOWN48:s:Dunbarton;
TOWN49:s:Emain Macha;
TOWN5:s:Tir Chonaill;
TOWN50:s:Tir Chonaill;
TOWN51:s:Tir Chonaill;
TOWN52:s:Taillteann;
TOWN53:s:Tir Chonaill;
TOWN54:s:Dunbarton;
TOWN55:s:Qilla;
TOWN56:s:Physis;
TOWN57:s:Emain Macha;
TOWN58:s:Filia;
TOWN59:s:Dunbarton;
TOWN6:s:Belvast;
TOWN60:s:Cobh;
TOWN7:s:Zardine;
TOWN8:s:Dunbarton;
TOWN9:s:Filia;
TYPE0:s:Healer's House;
TYPE1:s:Healer's House;
TYPE10:s:Blacksmith Shop;
TYPE11:s:Bank;
TYPE12:s:General;
TYPE13:s:General;
TYPE14:s:Adventurers' Association;
TYPE15:s:Puppet Shop;
TYPE16:s:Clothing Shop;
TYPE17:s:Clothing Shop;
TYPE18:s:General Shop;
TYPE19:s:Clothing Shop;
TYPE2:s:Healer's House;
TYPE20:s:General Shop;
TYPE21:s:Clothing Shop;
TYPE22:s:General Shop;
TYPE23:s:General Shop;
TYPE24:s:General Shop;
TYPE25:s:General Shop;
TYPE26:s:Clothing Shop;
TYPE27:s:Inn;
TYPE28:s:General Shop;
TYPE29:s:Street Artist;
TYPE3:s:Healer's House;
TYPE30:s:Grocery Store;
TYPE31:s:Pontiff's Court;
TYPE32:s:Flower Shop;
TYPE33:s:Alchemist House;
TYPE34:s:Library;
TYPE35:s:Grocery Store;
TYPE36:s:Stope;
TYPE37:s:Music Shop;
TYPE38:s:Jousting Arena;
TYPE39:s:Pub;
TYPE4:s:The Amazing Race of Mabinogi;
TYPE40:s:Church;
TYPE41:s:Church;
TYPE42:s:Grocery Store;
TYPE43:s:Church;
TYPE44:s:Blacksmith Shop;
TYPE45:s:Healer's House;
TYPE46:s:Church;
TYPE47:s:Grocery Store;
TYPE48:s:Church;
TYPE49:s:Grocery Store;
TYPE5:s:Healer's House;
TYPE50:s:Church;
TYPE51:s:Blacksmith Shop;
TYPE52:s:Church;
TYPE53:s:Grocery Store;
TYPE54:s:Bookstore;
TYPE55:s:Weapons Shop;
TYPE56:s:Weapons Shop;
TYPE57:s:Weapons Shop;
TYPE58:s:Weapons Shop;
TYPE59:s:Weapons Shop;
TYPE6:s:Bank;
TYPE60:s:Lighthouse;
TYPE7:s:Healer's House;
TYPE8:s:Healer's House;
TYPE9:s:General Shop;
```

- `ArbeitListSize : int32` - this contains the value N which is used for the list entries
- `SmartContentBoardType : string` - ???
- N `ENDTIME{0..N-1} : int32` entries
- N `STARTTIME{0..N-1} : int32` entries
- N `TOWN{0..N-1} : string` entries
- N `TYPE{0..N-1} : string` entries

These are then sorted and shoved into a one-line string, as shown in the packet dump.
