MabiNovel
=========

MabiNovelBulletin
-----------------

Novels in the bulletin seem to be sorted by expire date (from closest-to-expire to
farthest-to-expire).

Packet flow when opening the bulletin:
```
> Send MabiNovelBulletin   (0x65E1)
> Recv ???                 (0x65EC)
> Recv MabiNovelBulletinR  (0x65E2)
```

Request example:
```
Op: 000065E1, Id: 00100000000XXXXX

<empty>
```

Response example:
```
Op: 000065EC, Id: 0010000000XXXXXX

00001 [........00000000] Int    : 0
```

```
Op: 000065E2, Id: 00100000000XXXXX

001 [........0000009C] Int    : 156  // Entry count
// Entry 0
002 [00000000000003FC] Long   : 1020                          // Id
003 [0000000000000000] Long   : 0                             //
004 [................] String : Sabina                        // Author
005 [................] String : Detective Investigation Log 1 // Name
006 [........00000299] Int    : 665                           // Transcriptions
007 [000039D9A2D2A400] Long   : 63606902400000                // Expire date?
008 [........00000003] Int    : 3                             //
009 [........00000000] Int    : 0                             //
010 [........00000657] Int    : 1623                          // Views
// Entry 1
011 [00000000000003FD] Long   : 1021
012 [0000000000000000] Long   : 0
013 [................] String : Sabina
014 [................] String : Detective Investigation Log 2
015 [........0000003A] Int    : 58
016 [000039D9A2D2A400] Long   : 63606902400000
017 [........00000000] Int    : 0
018 [........00000000] Int    : 0
019 [........000004F7] Int    : 1271
// ...
```


MabiNovelTranscribe
-------------------

Request example:
```
Op: 000065E3, Id: 00100000000XXXXX

001 [00000000000003FC] Long   : 1020      // Id
002 [................] String : 127.0.0.1 // Localhost IP?
```

Response example:
```
Op: 000065E4, Id: 00100000000XXXXX

001 [00000000000003FC] Long   : 1020 // Id
002 [........00000320] Int    : 800  // Updated transcription count
```

If an error occurs, it may respond with a messagebox instead:
```
Op: 0000526F, Id: 0010000000XXXXXX

00001 [................] String : Failed due to an unknown reason.
Please try again later.
00002 [..............00] Byte   : 0
00003 [..............01] Byte   : 1
00004 [..............01] Byte   : 1
```


MabiNovelRoyalties
------------------

Request example:
```
Op: 000065E5, Id: 00100000000XXXXX

<empty>
```

If not an author, may instead immediately respond with a MsgBox packet:
```
Op: 0000526F, Id: 00100000000XXXXX

001 [................] String : You are not a MabiNovel author.
Please check before trying again.
002 [..............00] Byte   : 0
003 [..............01] Byte   : 1
004 [..............01] Byte   : 1
```


MabiNovelSubmit
---------------

Request:
```
Op: 000065DD, Id: 0010000000XXXXXX

00001 [................] String : Test Novel of Wisdom  // Novel title
00002 [........00000002] Int    : 2                     // Entry count
// Entry[0]
00003 [............0000] Short  : 0                     // Page number
00004 [............0004] Short  : 4                     // Background Id
00005 [............0002] Short  : 2                     // Background music Id
00006 [............00A7] Short  : 167                   // Portrait Id
00007 [............0000] Short  : 0                     // Portrait position
00008 [............0002] Short  : 2                     // Emotion Id
00009 [............0001] Short  : 1                     // Sound effect Id
00010 [............0000] Short  : 0                     // Effect Id
00011 [................] String : Hello! This is a test MabiNovel. Please don't read it!  // Text
// Entry[1]
00012 [............0001] Short  : 1
00013 [............0039] Short  : 57
00014 [............0028] Short  : 40
00015 [............0019] Short  : 25
00016 [............0001] Short  : 1
00017 [............0000] Short  : 0
00018 [............0004] Short  : 4
00019 [............0004] Short  : 4
00020 [................] String : Beep boop.
```

So far I have only gotten an error messagebox as a response:
```
Op: 0000526F, Id: 0010000000XXXXXX

00001 [................] String : You are not a MabiNovel author.
Please check before trying again.
00002 [..............00] Byte   : 0
00003 [..............01] Byte   : 1
00004 [..............01] Byte   : 1
```


MabiNovelView
-------------

Request example:
```
Op: 000065DF, Id: 00100000000XXXXX

001 [00000000000003FC] Long   : 1020 // Id of requested MabiNovel
```

Response example:
```
Op: 000065E0, Id: 00100000000XXXXX

001 [........00000018] Int    : 24
// Entry 0
002 [............0000] Short  : 0
003 [............0000] Short  : 0
004 [............0001] Short  : 1
005 [............0000] Short  : 0
006 [............0000] Short  : 0
007 [............0000] Short  : 0
008 [............0000] Short  : 0
009 [............0000] Short  : 0
010 [................] String : (I am Detective J, a professional detective that investigates the mysteries of Dunbarton.)
// Entry 1
011 [............0001] Short  : 1
012 [............0000] Short  : 0
013 [............0001] Short  : 1
014 [............0000] Short  : 0
015 [............0000] Short  : 0
016 [............0000] Short  : 0
017 [............0000] Short  : 0
018 [............0000] Short  : 0
019 [................] String : (This case has 10 suspects. These are the interrogation records of Ferghus, one of the suspects.) 
020 [............0002] Short  : 2
021 [............0039] Short  : 57
022 [............0055] Short  : 85
023 [............011B] Short  : 283
024 [............0000] Short  : 0
025 [............0001] Short  : 1
026 [............0000] Short  : 0
027 [............0000] Short  : 0
028 [................] String : So, where were you at the time in question? 
029 [............0003] Short  : 3
030 [............0039] Short  : 57
031 [............0055] Short  : 85
032 [............0110] Short  : 272
033 [............0001] Short  : 1
034 [............0000] Short  : 0
035 [............0000] Short  : 0
036 [............0000] Short  : 0
037 [................] String : Why are you asking me this again? I told you I was making the Gathering Axe that I got an order for. 
038 [............0004] Short  : 4
039 [............0039] Short  : 57
040 [............0055] Short  : 85
041 [............0110] Short  : 272
042 [............0001] Short  : 1
043 [............0001] Short  : 1
044 [............0000] Short  : 0
045 [............0000] Short  : 0
046 [................] String : I am a master blacksmith! Everybody wants me to work on their equipment! 
047 [............0005] Short  : 5
048 [............0039] Short  : 57
049 [............0055] Short  : 85
050 [............0110] Short  : 272
051 [............0001] Short  : 1
052 [............0001] Short  : 1
053 [............0000] Short  : 0
054 [............0000] Short  : 0
055 [................] String : It was a chilly night, and Eweca was shining brightly, like it was blessing my hammer or something. 
056 [............0006] Short  : 6
057 [............0039] Short  : 57
058 [............0055] Short  : 85
059 [............011B] Short  : 283
060 [............0000] Short  : 0
061 [............0001] Short  : 1
062 [............0000] Short  : 0
063 [............0000] Short  : 0
064 [................] String : Eweca? You were smithing in the middle of the night? 
065 [............0007] Short  : 7
066 [............0039] Short  : 57
067 [............0055] Short  : 85
068 [............0110] Short  : 272
069 [............0001] Short  : 1
070 [............0001] Short  : 1
071 [............0000] Short  : 0
072 [............0000] Short  : 0
073 [................] String : I was creating art! A masterpiece! I kept one as a souvenir if you want to see. 
074 [............0008] Short  : 8
075 [............0039] Short  : 57
076 [............0055] Short  : 85
077 [............0000] Short  : 0
078 [............0000] Short  : 0
079 [............0000] Short  : 0
080 [............0000] Short  : 0
081 [............0000] Short  : 0
082 [................] String : (This handle is completely bent. And what about this stubby excuse for a blade? The shape makes it look like a hoe more than an axe.) 
083 [............0009] Short  : 9
084 [............0039] Short  : 57
085 [............0055] Short  : 85
086 [............0110] Short  : 272
087 [............0001] Short  : 1
088 [............0001] Short  : 1
089 [............0000] Short  : 0
090 [............0000] Short  : 0
091 [................] String : I was so proud of my work that I delivered it to the Dugald Aisle Logging Camp myself! 
092 [............000A] Short  : 10
093 [............0039] Short  : 57
094 [............0055] Short  : 85
095 [............011B] Short  : 283
096 [............0000] Short  : 0
097 [............0001] Short  : 1
098 [............0000] Short  : 0
099 [............0000] Short  : 0
100 [................] String : Why would you do this all in the middle of the night? And you said Dugald Aisle Logging Camp? So the person who ordered the axe must be Tracy. 
101 [............000B] Short  : 11
102 [............0039] Short  : 57
103 [............0055] Short  : 85
104 [............0110] Short  : 272
105 [............0001] Short  : 1
106 [............0001] Short  : 1
107 [............0000] Short  : 0
108 [............0000] Short  : 0
109 [................] String : Yes. He may seem dull on the outside, but that guy really knows how to pick an axe. 
110 [............000C] Short  : 12
111 [............0005] Short  : 5
112 [............0055] Short  : 85
113 [............0106] Short  : 262
114 [............0001] Short  : 1
115 [............0001] Short  : 1
116 [............0000] Short  : 0
117 [............0000] Short  : 0
118 [................] String : Oh, hello Ferghus! Is this the axe I ordered? Let me see. This feels way better than my old axe. It has a perfect grip. You always amaze me, Ferghus! 
119 [............000D] Short  : 13
120 [............0039] Short  : 57
121 [............0055] Short  : 85
122 [............0110] Short  : 272
123 [............0001] Short  : 1
124 [............0001] Short  : 1
125 [............0000] Short  : 0
126 [............0000] Short  : 0
127 [................] String : That was all he said. He seemed really happy. 
128 [............000E] Short  : 14
129 [............0039] Short  : 57
130 [............0055] Short  : 85
131 [............0000] Short  : 0
132 [............0000] Short  : 0
133 [............0000] Short  : 0
134 [............0000] Short  : 0
135 [............0000] Short  : 0
136 [................] String : (It was probably too dark to see...) 
137 [............000F] Short  : 15
138 [............0039] Short  : 57
139 [............0055] Short  : 85
140 [............011B] Short  : 283
141 [............0000] Short  : 0
142 [............0001] Short  : 1
143 [............0000] Short  : 0
144 [............0000] Short  : 0
145 [................] String : Did you return to Tir Chonaill right after delivering the axe? 
146 [............0010] Short  : 16
147 [............0039] Short  : 57
148 [............0055] Short  : 85
149 [............0110] Short  : 272
150 [............0001] Short  : 1
151 [............0001] Short  : 1
152 [............0000] Short  : 0
153 [............0000] Short  : 0
154 [................] String : Well, a few days before, Simon in Dunbarton had ordered a bunch of needles. I delivered those as well. 
155 [............0011] Short  : 17
156 [............0039] Short  : 57
157 [............0055] Short  : 85
158 [............0110] Short  : 272
159 [............0001] Short  : 1
160 [............0001] Short  : 1
161 [............0000] Short  : 0
162 [............0000] Short  : 0
163 [................] String : Most blacksmiths deal with big hunks of metal, but when you reach my level, you can make something as delicate as a needle. 
164 [............0012] Short  : 18
165 [............0039] Short  : 57
166 [............0055] Short  : 85
167 [............0110] Short  : 272
168 [............0001] Short  : 1
169 [............0001] Short  : 1
170 [............0000] Short  : 0
171 [............0000] Short  : 0
172 [................] String : I was not inspired by the Eweca as one would imagine, but it was still a very sturdy needle. However... 
173 [............0013] Short  : 19
174 [............0004] Short  : 4
175 [............0055] Short  : 85
176 [............0092] Short  : 146
177 [............0001] Short  : 1
178 [............0004] Short  : 4
179 [............0000] Short  : 0
180 [............0000] Short  : 0
181 [................] String : Oh, no, Mr. Ferghus! I need a sewing needle to make elegant dresses with. Not a fishing hook... 
182 [............0014] Short  : 20
183 [............0039] Short  : 57
184 [............0055] Short  : 85
185 [............0110] Short  : 272
186 [............0001] Short  : 1
187 [............0001] Short  : 1
188 [............0000] Short  : 0
189 [............0000] Short  : 0
190 [................] String : Can you believe it? A complaint! I mean, my hands did slip a few times, but it was still a perfectly functional needle. 
191 [............0015] Short  : 21
192 [............0039] Short  : 57
193 [............0055] Short  : 85
194 [............0000] Short  : 0
195 [............0000] Short  : 0
196 [............0000] Short  : 0
197 [............0000] Short  : 0
198 [............0000] Short  : 0
199 [................] String :  (A perfectly functional needle? Poor, poor Simon.) 
200 [............0016] Short  : 22
201 [............0039] Short  : 57
202 [............0055] Short  : 85
203 [............0000] Short  : 0
204 [............0000] Short  : 0
205 [............0000] Short  : 0
206 [............0000] Short  : 0
207 [............0000] Short  : 0
208 [................] String : (Well, his story may be exaggerated, but I do not believe he is lying.) 
209 [............0017] Short  : 23
210 [............0039] Short  : 57
211 [............0055] Short  : 85
212 [............0000] Short  : 0
213 [............0000] Short  : 0
214 [............0000] Short  : 0
215 [............0000] Short  : 0
216 [............0000] Short  : 0
217 [................] String : (All three of their alibis appear to be solid.)
// Footer?
218 [00000000000003FC] Long   : 1020 // Id
219 [........00000658] Int    : 1624 // Updated view count
```


Transcribed Novels
------------------

Packet flow when using a transcribed mabinovel:
```
> Send UseItem        (0x59EB)
> Recv UseItemR       (0x59EC)
> Recv MabiNovelViewR (0x65E0)
```

The UseItemR response:
```
Op: 000059EC, Id: 00100000000XXXXX

001 [..............01] Byte   : 1
002 [........000006A4] Int    : 1700
```


Other Packets
-------------

Sent inbetween MabiNovelBulletin (0x65E1) and MabiNovelBulletinR (0x65E2):
Might indicate whether or not the user is an author?
```
Op: 000065EC, Id: 00100000000XXXXX

001 [........00000000] Int    : 0
```
